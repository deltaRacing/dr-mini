DR-Mini Firmware for nodeMCU

## Pin map

D0 = onboard LED (Blau), invertiert<br>
D1 = Motor Freigabe<br>
D2 = Motor Drehrichtung<br>
D5 = Abblendlicht (LED Weiß)<br>
D6 = Bremslicht (LED Rot)<br>

## IP Settings

**Port**: 2403<br>
**Server IP**: 192.168.4.1 (default)<br>

## Commands
Jeder Command (Befehl) wird durch \r\n abgeschlossen

`MOT_ON` - Motor aktivieren<br>
`MOT_OFF` - Motor deaktivieren<br>
`MOT_F` - Motor Drehrichtung vorwärts<br>
`MOT_B` - Motor Drehrichtung rückwärts<br>
`LIGHT_ON` - Abblendlicht an<br>
`LIGHT_OFF` - Abblendlicht aus<br>
`BRAKELIGHT_ON` - Bremslicht an<br>
`BRAKELIGHT_OFF` - Bremslicht aus<br>
`SPEED_XXX` - Motordrehzahl XXX%<br>