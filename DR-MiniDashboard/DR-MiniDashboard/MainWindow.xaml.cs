﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Net.Sockets;

namespace DR_MiniDashboard
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        Socket socket;
        bool connected = false;
        bool continousUpdate = false;

        bool brakelight = false;
        bool light = false;
        bool motor = false;

        public MainWindow()
        {
            InitializeComponent();

            base.Closing += closeEvent;
        }

        void closeEvent(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if(connected)
                disconnect();
        }

        private void connect()
        {
            try
            {
                socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                socket.Connect("192.168.4.1", 2403);

                if (connected = socket.Connected)
                    RemoteControls.IsEnabled = true;

            }
            catch (Exception e)
            {
                Console.Write(e);
            }

        }

        private void disconnect()
        {
            if (!connected)
                return;

            try
            {
                socket.Send(System.Text.Encoding.UTF8.GetBytes("quit\r\n"));
                socket.Disconnect(false);

                if (!(connected = socket.Connected))
                    RemoteControls.IsEnabled = false;

                socket.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        private void sendCommand(String command)
        {
            try
            {
                if(connected)
                {
                    Byte[] tx = System.Text.Encoding.UTF8.GetBytes(command + "\r\n");
                    socket.Send(tx);
                }

            }
            catch (Exception e)
            {
                Console.Write(e.Message);
            }

        }

        private void buttonMotorOn_Click(object sender, RoutedEventArgs e)
        {
            sendCommand("MOT_ON");
        }

        private void buttonMotorOff_Click(object sender, RoutedEventArgs e)
        {
            sendCommand("MOT_OFF");
        }

        private void buttonMotorForward_Click(object sender, RoutedEventArgs e)
        {
            sendCommand("MOT_F");
        }

        private void buttonMotorBackward_Click(object sender, RoutedEventArgs e)
        {
            sendCommand("MOT_B");
        }

        private void buttonLightOn_Click(object sender, RoutedEventArgs e)
        {
            sendCommand("LIGHT_ON");
        }

        private void buttonLightOff_Click(object sender, RoutedEventArgs e)
        {
            sendCommand("LIGHT_OFF");
        }

        private void buttonBrakelightOn_Click(object sender, RoutedEventArgs e)
        {
            sendCommand("BRAKELIGHT_ON");
        }

        private void buttonBrakelightOff_Click(object sender, RoutedEventArgs e)
        {
            sendCommand("BRAKELIGHT_OFF");
        }

        private void buttonConnect_Click(object sender, RoutedEventArgs e)
        {
            if (connected)
                disconnect();
            else
                connect();
        }

        private void speedSlider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            sendCommand("SPEED_" + ((int)speedSlider.Value).ToString());

            if (continousUpdate)
                sendCommand("MOT_ON");
        }

        private void continousUpdate_on(object sender, RoutedEventArgs e)
        {
            continousUpdate = true;
        }

        private void continousUpdate_off(object sender, RoutedEventArgs e)
        {
            continousUpdate = false;
        }

        private void Window_KeyAction(object sender, KeyEventArgs e)
        {
            switch (e.Key)
            {
                case Key.Up:
                    if (e.KeyStates == KeyStates.Down)
                    {
                        if (motor == false)
                        {
                            motor = true;
                            sendCommand("MOT_F");
                            sendCommand("MOT_ON");
                        }
                    }
                    else
                    {
                        if (motor == true)
                        {
                            motor = false;
                            sendCommand("MOT_OFF");
                        }
                    }
                    break;

                case Key.Down:
                    if (e.KeyStates == KeyStates.Down)
                    {
                        if (motor == false)
                        {
                            motor = true;
                            sendCommand("MOT_B");
                            sendCommand("MOT_ON");
                        }

                    }
                    else
                    {
                        if (motor == true)
                        {
                            motor = false;
                            sendCommand("MOT_OFF");
                        }
                    }
                    break;

                case Key.Space:
                    if (e.KeyStates == KeyStates.Down)
                    {
                        if (brakelight == false)
                        {
                            brakelight = true;
                            sendCommand("BRAKELIGHT_ON");
                        }
                    }
                    else
                    {
                        if (brakelight == true)
                        {
                            brakelight = false;
                            sendCommand("BRAKELIGHT_OFF");
                        }
                    }
                    break;

                case Key.L:
                    if (e.KeyStates == KeyStates.Down)
                    {
                        if (light == false)
                        {
                            light = true;
                            sendCommand("LIGHT_ON");
                        }
                    }
                    else
                    {
                        if (light == true)
                        {
                            light = false;
                            sendCommand("LIGHT_OFF");
                        }
                    }
                    break;
            }


        }
    }
}
