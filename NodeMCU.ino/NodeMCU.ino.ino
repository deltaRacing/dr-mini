/*
 * Copyright (c) 2015, Majenko Technologies
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 * 
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * 
 * * Redistributions in binary form must reproduce the above copyright notice, this
 *   list of conditions and the following disclaimer in the documentation and/or
 *   other materials provided with the distribution.
 * 
 * * Neither the name of Majenko Technologies nor the names of its
 *   contributors may be used to endorse or promote products derived from
 *   this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/* Create a WiFi access point and provide a web server on it. */

#include <ESP8266WiFi.h>
#include <WiFiServer.h>


#define CONLED  D0
/* Motorsteuerung
 * Pin D1 = Motor AN/AUS
 * Pin D2 = Motor Vorwärts/Rückwärts
 * Pin D5 = Abblendlicht AN/AUS
 * Pin D6 = Bremslicht AN/AUS
 */
#define MOTOR     D1
#define DIRECTION D2
#define LIGHT     D5
#define BRAKE     D6

#define OFF 0
#define ON  1
#define FORWARD  0
#define BACKWARD 1

#define MOTOR_MAX 1023.0
#define MOTOR_MIN 223.0

/* Set these to your desired credentials. */
const char *ssid = "DR-Mini9";
const char *password = "";

WiFiServer server(2403);
String buf = "\0";
int pwmVal = MOTOR_MAX;

void setup() {

  //Aktiviere Diagnose ausgaben
  WiFi.printDiag(Serial);

  pinMode(CONLED, OUTPUT);
  digitalWrite(CONLED, HIGH);

  //Steuerausgänge für Motor und Lichter einstellen
  pinMode(MOTOR, OUTPUT);
  pinMode(DIRECTION, OUTPUT);
  pinMode(LIGHT, OUTPUT);
  pinMode(BRAKE, OUTPUT);
  
  //Motor ausschalten und vorwärtsgange einlegen
  //digitalWrite(MOTOR, OFF);
  analogWrite(MOTOR, OFF);
  digitalWrite(DIRECTION, FORWARD);

  //Licht ausschalten
  digitalWrite(LIGHT, OFF);
  digitalWrite(BRAKE, OFF);
  
	delay(1000);
	Serial.begin(115200);
	Serial.println();
	Serial.println("Konfiguiere W-LAN Zugriff..."); 

	WiFi.softAP(ssid, password);

	IPAddress myIP = WiFi.softAPIP();
  Serial.print("SSID: ");
  Serial.println(ssid);
	Serial.print("DR-Mini IP-Adresse: ");
	Serial.println(myIP);
	
	server.begin();
	
	Serial.println("Steuergeraet bereit");
}

void loop() {
	//Warte auf eingehende Verbindungen
  WiFiClient client = server.available();

  if (client) {

    //Ein Smartphone hat sich verbunde
    if (client.connected()) {
      digitalWrite(CONLED, LOW);
      
      Serial.println("Steuerung verbunden");
  
      client.println("DR-Mini Control 1.0 - Exit with \'quit\'");

      do{

        if(!client.connected())
          break;
        
        //Kommando von Steuerung lesen
        buf = client.readStringUntil('\n');

        //Testausgabe für Socket Empfang
        //Serial.print(buf);
        
        if(buf.length() <= 1)
          continue;

        //Entferne \r aus Kommando, \r ist das letzte Zeichen im String
        buf.remove(buf.length()-1);
        
        //Kommando zum Überprüfen über USB ausgeben
        //Serial.println(buf);

        //Kommando interpretieren und Aktion ausführen
        if(buf.equals("MOT_F"))
        {
          digitalWrite(DIRECTION, FORWARD);
          Serial.println("Vorwaerts");
        }
        else if(buf.equals("MOT_B"))
        {
          digitalWrite(DIRECTION, BACKWARD);
          Serial.println("Rueckwaerts");
        } 
        else if(buf.equals("MOT_ON"))
        {
          //digitalWrite(MOTOR, ON);
          analogWrite(MOTOR, pwmVal);
          Serial.println("Motor AN");
        }
        else if(buf.equals("MOT_OFF"))
        {
          //digitalWrite(MOTOR, OFF);
          analogWrite(MOTOR, OFF);
          Serial.println("Motor AUS");
        }
        else if(buf.equals("LIGHT_ON"))
        {
          digitalWrite(LIGHT, ON);
          Serial.println("Licht AN");
        }
        else if(buf.equals("LIGHT_OFF"))
        {
          digitalWrite(LIGHT, OFF);
          Serial.println("Licht AUS");
        }
        else if(buf.equals("BRAKELIGHT_ON"))
        {
          digitalWrite(BRAKE, ON);
          Serial.println("Bremslicht AN");
        }
        else if(buf.equals("BRAKELIGHT_OFF"))
        {
          digitalWrite(BRAKE, OFF);
          Serial.println("Bremslicht AUS");
        }
        else if(buf.indexOf("SPEED_") == 0)
        {
          String input = buf.substring(6);
          int tmp = input.toInt() * ((MOTOR_MAX - MOTOR_MIN)/100) + MOTOR_MIN;
          
          if(tmp <= MOTOR_MAX)
          {
            pwmVal = tmp;
            Serial.print("PWM DC: ");
            Serial.println(pwmVal);
          }
          else
          {
            Serial.println("Drehzahlwert ungueltig!");
          }
                   
        }
          
      }while(buf != "quit");
      
    }

    // close the connection:
    client.stop();
    digitalWrite(CONLED, HIGH);
    Serial.println("Steuerung getrennt");
  }

  delay(1);
}
